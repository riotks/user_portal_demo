**Eeldused:** 
-Java 8
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

-Node.js
https://nodejs.org/

## Käivitamine

Proovitöö käivitamiseks esmalt paki zip lahti soovitud kohta. Lahtipakitud projekti sõltuvuste paigaldamiseks ja tööde käivitamiseks:

Serveri käivitamiseks käsurealt cd 'server' kausta ning jooksuta järgmine käsk:
 
```
mvnw spring-boot:run
```

Kliendi käivitamiseks käsurealt cd 'client' kausta ning jooksuta järgmine käsk:
 
```
npm install && npm start
```

Rakendust saab avada sirvijas aadressil 'localhost:4200'.

H2 baasile saab ligi localhost:8080/h2-console
u: sa
pw: 
