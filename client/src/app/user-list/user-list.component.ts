import { Component, OnInit } from '@angular/core';
import { UserService, GiphyService } from '../shared';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserService, GiphyService]
})
export class UserListComponent implements OnInit {
  users: Array<any>;

  constructor(private userService: UserService, private giphyService: GiphyService) { }

  ngOnInit() {
    this.userService.getAll().subscribe(
      data => {
        this.users = data;
        for (const user of this.users) {
          this.giphyService.get(user.name).subscribe(url => user.giphyUrl = url);
        }
      },
      error => console.log(error)
    );
  }
}
