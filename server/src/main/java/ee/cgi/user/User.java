package ee.cgi.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
class User {

	@Id
	@GeneratedValue
	private Long id;
	private String firstName;
	private String lastName;
	private String name;
	private String email;
	private Date dateOfBirth;
	private String phone;

	public String getName() {
		return firstName + " " + lastName;
	}

	//TODO adding new users by name. Or make a full form.
	public void setName(String name) {
		if (name.isEmpty()) {
			return;
		}

		String[] array = name.trim().split(" ", 1);
		this.firstName = array[0];
		this.lastName = array[1];
	}
}