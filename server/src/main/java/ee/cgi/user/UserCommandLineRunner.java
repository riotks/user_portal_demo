package ee.cgi.user;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class UserCommandLineRunner implements CommandLineRunner {

	private final UserRepository repository;

	public UserCommandLineRunner(UserRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception {
		List<User> userList = getTestList();

		userList.forEach(repository::save);

		repository.findAll().forEach(System.out::println);
	}

	private List<User> getTestList() {
		List<User> userList = new LinkedList<>();
		userList.add(User.builder()
				.firstName("Borat")
				.lastName("Sagdiyev")
				.email("potassiumboy.1@yahoo.kz")
				.phone("+7 55 66 77 88")
				.build());

		userList.add(User.builder()
				.firstName("Power")
				.lastName("Cord")
				.email("electric@shocking.ch")
				.phone("+372 55 8888 77")
				.build());

		userList.add(User.builder()
				.firstName("Flex")
				.lastName("Claws")
				.email("kitty@ipsum.com")
				.phone("+2432 324 2342")
				.build());

		userList.add(User.builder()
				.firstName("Chase")
				.lastName("Lazer")
				.email("or.lazer.ch@se.yu")
				.phone("+33 333 333 333")
				.build());

		userList.add(User.builder()
				.firstName("Cats")
				.lastName("Love")
				.email("2scream@4.am")
				.phone("+4 444444444")
				.build());
		return userList;
	}
}