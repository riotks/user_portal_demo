package ee.cgi.user;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class UserController {
    private UserRepository repository;

    public UserController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/user-list")
    @CrossOrigin(origins = "http://localhost:4200")
    public Collection<User> validUsers() {

        return repository.findAll().stream()
                .filter(this::isNotShocking)
                .collect(Collectors.toList());
    }

    private boolean isNotShocking(User user) {
        return !user.getName().contains("Power");
    }
}